;;; qmk-mode.el --- minor mode for configuring qmk keyboards -*- lexical-binding: t -*-

;; Copyright (C) 2022 popska

;; Author: popska <popska@tuta.io>
;; URL: https://gitlab.com/popska/qmk-mode
;; Version: 1.0
;; Package-Requires: ((emacs "27.1"))
;; Keywords: keyboard qmk

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <https://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; This package aims to make configuring QMK keyboards easier. It
;; allows for editing the keys directly from the comment typically
;; present above the array containing the actual layout. This allows
;; for easier and more simple syntax; for example M-x instead of
;; A(KC_X). qmk-mode also assists in traversing and editing this
;; comment.

;;; Code:


(require 's)


(defvar qmk-keyboard-table nil
  "A string of the table with numbers (starting from 0) to indicate
where to place the keycodes in the matrix")


(defvar qmk-modifier-mask-alist
  '(("C" . "MOD_MASK_CTRL")
    ("S" . "MOD_MASK_SHIFT")
    ("A" . "MOD_MASK_ALT")
    ("G" . "MOD_MASK_GUI")))


(defvar qmk-modifier-code-alist
  '(("C" . "KC_LCTL")
    ("S" . "KC_LSFT")
    ("A" . "KC_LALT")
    ("G" . "KC_LGUI")
    ("M" . "KC_MEH")
    ("H" . "KC_HYPR")))


(defvar qmk-modifier-wrap-alist
  '(("C" . "C")
    ("S" . "S")
    ("A" . "A")
    ("G" . "G")
    ("M" . "MEH")
    ("H" . "HYPR")))


(defvar qmk-basic-code-alist
  '(
    ;; letters
    ("a" . "KC_A")
    ("b" . "KC_B")
    ("c" . "KC_C")
    ("d" . "KC_D")
    ("e" . "KC_E")
    ("f" . "KC_F")
    ("g" . "KC_G")
    ("h" . "KC_H")
    ("i" . "KC_I")
    ("j" . "KC_J")
    ("k" . "KC_K")
    ("l" . "KC_L")
    ("m" . "KC_M")
    ("n" . "KC_N")
    ("o" . "KC_O")
    ("p" . "KC_P")
    ("q" . "KC_Q")
    ("r" . "KC_R")
    ("s" . "KC_S")
    ("t" . "KC_T")
    ("u" . "KC_U")
    ("v" . "KC_V")
    ("w" . "KC_W")
    ("x" . "KC_X")
    ("y" . "KC_Y")
    ("z" . "KC_Z")

    ;; numbers
    ("0" . "KC_0")
    ("1" . "KC_1")
    ("2" . "KC_2")
    ("3" . "KC_3")
    ("4" . "KC_4")
    ("5" . "KC_5")
    ("6" . "KC_6")
    ("7" . "KC_7")
    ("8" . "KC_8")
    ("9" . "KC_9")

    ;; punctuation + special
    ("ent" . "KC_ENT")
    ("enter" . "KC_ENT")
    ("esc" . "KC_ESC")
    ("escape" . "KC_ESC")
    ("backspace" . "KC_BSPC")
    ("bspc" . "KC_BSPC")
    ("tab" . "KC_TAB")
    ("space" . "KC_SPC")
    ("spc" . "KC_SPC")
    ("minus" . "KC_MINS")
    ("mins" . "KC_MINS")
    ("-" . "KC_MINS")
    ("equal" . "KC_EQL")
    ("eql" . "KC_EQL")
    ("=" . "KC_EQL")
    ("left_bracket" . "KC_LBRC")
    ("lbrc" . "KC_LBRC")
    ("[" . "KC_LBRC")
    ("right_bracket" . "KC_RBRC")
    ("rbrc" . "KC_RBRC")
    ("]" . "KC_RBRC")
    ("backslash" . "KC_BSLS")
    ("bsls" . "KC_BSLS")
    ("\\" . "KC_BSLS")
    ("nonus_hash" . "KC_NUHS")
    ("nuhs" . "KC_NUHS")
    ("semicolon" . "KC_SCLN")
    ("scln" . "KC_SCLN")
    (";" . "KC_SCLN")
    ("quote" . "KC_QUOT")
    ("quot" . "KC_QUOT")
    ("'" . "KC_QUOT")
    ("grave" . "KC_GRV")
    ("grv" . "KC_GRV")
    ("`" . "KC_GRV")
    ("comma" . "KC_COMM")
    ("comm" . "KC_COMM")
    ("," . "KC_COMM")
    ("dot" . "KC_DOT")
    ("." . "KC_DOT")
    ("slash" . "KC_SLSH")
    ("slsh" . "KC_SLSH")
    ("/" . "KC_SLSH")
    ("nonus_backslash" . "KC_NUBS")
    ("nubs" . "KC_NUBS")

    ;; shifted symbols
    ;; these ones are useful with overrides
    ("tilde" . "KC_TILD")
    ("tild" . "KC_TILD")
    ("~" . "KC_TILD")
    ("exclaim" . "KC_EXLM")
    ("exlm" . "KC_EXLM")
    ("!" . "KC_EXLM")
    ("at" . "KC_AT")
    ("@" . "KC_AT")
    ("hash" . "KC_HASH")
    ("#" . "KC_HASH")
    ("dollar" . "KC_DLR")
    ("dlr" . "KC_DLR")
    ("$" . "KC_DLR")
    ("percent" . "KC_PERC")
    ("perc" . "KC_PERC")
    ("%" . "KC_PERC")
    ("circumflex" . "KC_CIRC")
    ("circ" . "KC_CIRC")
    ("^" . "KC_CIRC")
    ("ampersand" . "KC_AMPR")
    ("ampr" . "KC_AMPR")
    ("&" . "KC_AMPR")
    ("asterisk" . "KC_ASTR")
    ("astr" . "KC_ASTR")
    ("*" . "KC_ASTR")
    ("left_paren" . "KC_LPRN")
    ("lprn" . "KC_LPRN")
    ("(" . "KC_LPRN")
    ("right_paren" . "KC_RPRN")
    ("rprn" . "KC_RPRN")
    (")" . "KC_RPRN")
    ("underscore" . "KC_UNDS")
    ("unds" . "KC_UNDS")
    ("_" . "KC_UNDS")
    ("plus" . "KC_PLUS")
    ("+" . "KC_PLUS")
    ("left_curly_brace" . "KC_LCBR")
    ("lcbr" . "KC_LCBR")
    ("{" . "KC_LCBR")
    ("right_curly_brace" . "KC_RCBR")
    ("rcbr" . "KC_RCBR")
    ("}" . "KC_RCBR")
    ("pipe" . "KC_PIPE")
    ("\\|" . "KC_PIPE")
    ("colon" . "KC_COLN")
    ("coln" . "KC_COLN")
    (":" . "KC_COLN")
    ("double_quote" . "KC_DQUO")
    ("dquo" . "KC_DQUO")
    ("dqt" . "KC_DQT")
    ("\"" . "KC_DQUO")
    ("left_angle_bracket" . "KC_LT")
    ("lt" . "KC_LT")
    ("labk" . "KC_LABK")
    ("<" . "KC_LT")
    ("right_angle_bracket" . "KC_GT")
    ("gt" . "KC_GT")
    ("rabk" . "KC_RABK")
    (">" . "KC_GT")
    ("question" . "KC_QUES")
    ("ques" . "KC_QUES")
    ("?" . "KC_QUES")

    ;; special QMK codes
    ("no" . "KC_NO")
    ("xxx" . "XXXXXXX")
    ("transparent" . "KC_TRNS")
    ("trns" . "KC_TRNS")
    ("___" . "_______")

    ;; lock keys
    ("caps_lock" . "KC_CAPS")
    ("caps" . "KC_CAPS")
    ("scroll_lock" . "KC_SCRL")
    ("scrl" . "KC_SCRL")
    ("num_lock" . "KC_NUM")
    ("num" . "KC_NUM")
    ("locking_caps_lock" . "KC_LCAP")
    ("lcap" . "KC_LCAP")
    ("locking_num_lock" . "KC_LNUM")
    ("lnum" . "KC_LNUM")
    ("locking_scroll_lock" . "KC_LSCR")
    ("lscr" . "KC_LSCR")

    ;; commands
    ("print_screen" . "KC_PSCR")
    ("pscr" . "KC_PSCR")
    ("pause" . "KC_PAUS")
    ("paus" . "KC_PAUS")
    ("insert" . "KC_INS")
    ("ins" . "KC_INS")
    ("home" . "KC_HOME")
    ("page_up" . "KC_PGUP")
    ("pgup" . "KC_PGUP")
    ("delete" . "KC_DEL")
    ("del" . "KC_DEL")
    ("end" . "KC_END")
    ("page_down" . "KC_PGDN")
    ("pgdn" . "KC_PGDN")
    ("right" . "KC_RGHT")
    ("rght" . "KC_RGHT")
    ("left" . "KC_LEFT")
    ("down" . "KC_DOWN")
    ("up" . "KC_UP")
    ("application" . "KC_APP")
    ("app" . "KC_APP")

    ;; media keys
    ("system_power" . "KC_PWR")
    ("pwr" . "KC_PWR")
    ("system_sleep" . "KC_SLEP")
    ("slep" . "KC_SLEP")
    ("system_wake" . "KC_WAKE")
    ("wake" . "KC_WAKE")
    ("audio_mute" . "KC_MUTE")
    ("mute" . "KC_MUTE")
    ("audio_vol_up" . "KC_VOLU")
    ("volu" . "KC_VOLU")
    ("audio_vol_down" . "KC_VOLD")
    ("vold" . "KC_VOLD")
    ("media_next_track" . "KC_MNXT")
    ("mnxt" . "KC_MNXT")
    ("media_prev_track" . "KC_MPRV")
    ("mprv" . "KC_MPRV")
    ("media_stop" . "KC_MSTP")
    ("mstp" . "KC_MSTP")
    ("media_play_pause" . "KC_MPLY")
    ("mply" . "KC_MPLY")
    ("media_select" . "KC_MSEL")
    ("msel" . "KC_MSEL")
    ("media_eject" . "KC_EJCT")
    ("ejct" . "KC_EJCT")
    ("brightness_up" . "KC_BRIU")
    ("briu" . "KC_BRIU")
    ("brightness_down" . "KC_BRID")
    ("brid" . "KC_BRID")

    ;; num pad
    ("kp_slash" . "KC_PSLS")
    ("psls" . "KC_PSLS")
    ("kp_asterisk" . "KC_PAST")
    ("past" . "KC_PAST")
    ("kp_minus" . "KC_PMNS")
    ("pmns" . "KC_PMNS")
    ("kp_plus" . "KC_PPLS")
    ("ppls" . "KC_PPLS")
    ("kp_enter" . "KC_PENT")
    ("pent" . "KC_PENT")
    ("kp_0" . "KC_P0")
    ("p0" . "KC_P0")
    ("kp_1" . "KC_P1")
    ("p1" . "KC_P1")
    ("kp_2" . "KC_P2")
    ("p2" . "KC_P2")
    ("kp_3" . "KC_P3")
    ("p3" . "KC_P3")
    ("kp_4" . "KC_P4")
    ("p4" . "KC_P4")
    ("kp_5" . "KC_P5")
    ("p5" . "KC_P5")
    ("kp_6" . "KC_P6")
    ("p6" . "KC_P6")
    ("kp_7" . "KC_P7")
    ("p7" . "KC_P7")
    ("kp_8" . "KC_P8")
    ("p8" . "KC_P8")
    ("kp_9" . "KC_P9")
    ("p9" . "KC_P9")
    ("kp_dot" . "KC_PDOT")
    ("pdot" . "KC_PDOT")
    ("kp_equal" . "KC_PEQL")
    ("peql" . "KC_PEQL")
    ("kp_comma" . "KC_PCMM")
    ("pcmm" . "KC_PCMM")

    ;; f keys
    ("f1" . "KC_F1")
    ("f2" . "KC_F2")
    ("f3" . "KC_F3")
    ("f4" . "KC_F4")
    ("f5" . "KC_F5")
    ("f6" . "KC_F6")
    ("f7" . "KC_F7")
    ("f8" . "KC_F8")
    ("f9" . "KC_F9")
    ("f10" . "KC_F10")
    ("f11" . "KC_F11")
    ("f12" . "KC_F12")
    ("f13" . "KC_F13")
    ("f14" . "KC_F14")
    ("f15" . "KC_F15")
    ("f16" . "KC_F16")
    ("f17" . "KC_F17")
    ("f18" . "KC_F18")
    ("f19" . "KC_F19")
    ("f20" . "KC_F20")
    ("f21" . "KC_F21")
    ("f22" . "KC_F22")
    ("f23" . "KC_F23")
    ("f24" . "KC_F24")
    )
  "This maps strings to the KC_ keycodes. No uppercase letters in
the car allowed!")


(defun qmk--process-string (str)
  "Process STR using qmk-mode syntax and return QMK string to
insert"
  (let* ((case-fold-search nil)	; case is important for the regex searches
	 (mods (qmk--get-modifiers (s-trim str)))
	 (pair (assoc (qmk--try-override (alist-get 'infix mods))
		      qmk-basic-code-alist)))
    (if pair
	(concat
	 (alist-get 'prefix mods)
	 (cdr pair)
	 (alist-get 'suffix mods))
      (alist-get 'infix mods))))


(defun qmk--get-modifiers (str)
  "Takes an input string to parse and returns an alist containing 3
elements:

- prefix :: the modifier prefix keys which wrap the string, empty
  if none

- infix :: the string to apply the modifiers to (or the modifer
  KC_ code if it's just the mod)

- suffix :: some closing parens when needed"
  (let ((modwrapkey-rx `(any ,(apply 'concat (mapcar
					      (lambda (pair) (car pair))
					      qmk-modifier-wrap-alist)))))
    (if (not (string-match
	      (eval `(rx string-start (seq ,modwrapkey-rx "-")))
	      str))
	`((prefix . "")
	  (infix . ,(if (assoc str qmk-modifier-code-alist)
			(cdr (assoc str qmk-modifier-code-alist))
		      str))
	  (suffix . ""))
      (let ((next (qmk--get-modifiers (s-right (- (length str) 2) str))))
	`((prefix . ,(concat
		      (cdr (assoc (s-left 1 str) qmk-modifier-wrap-alist))
		      "("
		      (cdr (assoc 'prefix next))))
	  (infix . ,(cdr (assoc 'infix next)))
	  (suffix . ,(concat
		      ")"
		      (cdr (assoc 'suffix next)))))))))


(defun qmk--try-override (str)
  "Handles overriding STR when applicable. Returns STR sans any
override."
  (let ((delimiter-rx `(any ,(apply 'concat (mapcar
					     (lambda (pair) (car pair))
					     qmk-modifier-mask-alist)))))
    (if (string-match (eval `(rx (seq
			          (+ (not (any upper-case)))
			          ,delimiter-rx
			          (+ (not (any upper-case))))))
		      str)
	(let* ((keys (s-slice-at (eval `(rx ,delimiter-rx)) str))
	       (first (cdr (assoc (car keys)
				  qmk-basic-code-alist)))
	       (modmask (cdr (assoc (s-left 1 (cadr keys))
				    qmk-modifier-mask-alist)))
	       (last (cdr (assoc (s-right
				  (- (length (cadr keys)) 1)
				  (cadr keys))
				 qmk-basic-code-alist))))
	  ;; do the override, this assumes the **key_overrides array already exists
	  (qmk--insert-override first modmask last)
	  (car keys))
      str)))


(defun qmk--insert-override (first modmask last)
  (save-excursion
    (re-search-backward (rx "const key_override_t **key_overrides"))
    ;; try to get rid of the override if it exists
    (save-excursion
      (when (re-search-backward (eval `(rx (seq
				            "ko_make_basic("
				            (+ not-newline)
				            ","
				            (* " ")
				            ,first)))
				nil t)
	(beginning-of-line)
	(kill-line 1)))
    ;; Add the override variable
    (previous-line)
    (re-search-backward (rx (not (any space))))
    (end-of-line)
    (newline)
    (insert
     (format "const key_override_t %s_ovr = ko_make_basic(%s, %s, %s);"
	     (s-downcase first)
	     modmask
	     first
	     last))
    ;; add it to the override array
    (re-search-forward "NULL")
    (previous-line)
    (end-of-line)
    (newline)
    (insert (format "&%s_ovr," (s-downcase first)))
    (indent-region (line-beginning-position) (point))))


(defun qmk--count-prior-pipes ()
  "Counts the number of (nonescaped) pipes up until point"
  (re-search-backward (rx (or (seq (not "\\") "|") "/*")))
  (if (looking-at (rx "/*"))
      0
    (+ 1 (qmk--count-prior-pipes))))


(defun qmk--get-pos-in-matrix (count)
  "Gets the position of the current cell in the matrix based on the
COUNT of pipes before the number"
  (with-temp-buffer
    (insert qmk-keyboard-table)
    (goto-char (point-min))
    (dotimes (_ count)
      (search-forward "|"))
    (re-search-forward "[[:digit:]]+")
    (string-to-number (match-string 0))))


(defun qmk--insert-str-in-matrix (str pos)
  "Inserts STR in the matrix following point at position POS"
  (re-search-forward (rx (seq
			  "*/"
			  (*? anything)
			  (seq
			   (+? (any "A-Z" "a-z" "_"))
			   "("))))
  ;; this will break if there are any comments in the layout
  (dotimes (_ pos) (search-forward ","))
  (forward-word) (backward-word) 	; a bit hacky
  (while (not (looking-at (rx (or space ","))))
    (delete-char 1))
  (insert str)
  (message "inserted %s at position %d" str pos))


(defun qmk-cell-to-matrix ()
  "Process the string in the current cell and insert it in the
matrix"
  (interactive)
  (save-excursion
    (qmk--insert-str-in-matrix
     (qmk--process-string
      (buffer-substring
       (+ (prog1 (re-search-backward "[^\\]|") (forward-char)) 2)
       (- (prog1 (re-search-forward "[^\\]|") (backward-char 2)) 1)))
     (qmk--get-pos-in-matrix (qmk--count-prior-pipes)))))


(defun qmk-edit-cell-at-point ()
  "Edit the cell in the keyboard table at point"
  (interactive)
  (when (qmk--in-cell-p)
    (qmk--goto-string-in-cell)
    (let ((beg (prog1 (+ (re-search-backward "[^\\]|") 2) (forward-char)))
	  (end (prog1 (- (re-search-forward "[^\\]|") 1) (backward-char 2))))
      (delete-region beg end)
      (insert (s-center (- end beg) (read-string "enter string: "))))
    (qmk-cell-to-matrix)))


(defun qmk-forward-cell (arg)
  "Go to the cell to the right. If prefix ARG is given, then also
edit that cell."
  (interactive "P")
  (qmk--try-eval-til-cell '(re-search-forward "[^\\]|" (line-end-position) t))
  (when arg (call-interactively 'qmk-edit-cell-at-point)))


(defun qmk-backward-cell (arg)
  "Go to the cell to the left. If prefix ARG is given, then also
edit that cell."
  (interactive "P")
  (qmk--try-eval-til-cell '(re-search-backward "[^\\]|" (line-beginning-position) t))
  (when arg (call-interactively 'qmk-edit-cell-at-point)))


(defun qmk-up-cell (arg)
  "Go to the cell above. If prefix ARG is given, then also edit
that cell."
  (interactive "P")
  (let ((min (save-excursion (re-search-backward (rx "/*")))))
    (qmk--try-eval-til-cell '(progn
			       (previous-line)
			       (> (point) min))))
  (when arg (call-interactively 'qmk-edit-cell-at-point)))


(defun qmk-down-cell (arg)
  "Go to the cell down. If prefix ARG is given, then also edit that
cell."
  (interactive "P")
  (let ((min (save-excursion (re-search-backward (rx "/*")))))
    (qmk--try-eval-til-cell '(progn
			       (next-line)
			       (> (point) min))))
  (when arg (call-interactively 'qmk-edit-cell-at-point)))


(defun qmk--try-eval-til-cell (code)
  "Tries to evaluate CODE until either CODE returns nil or point is
in a cell"
  (while (and (eval code)
	      (not (qmk--in-cell-p)))))


(defun qmk--in-cell-p ()
  "Is point currently in a cell?"
  (and (not (or (looking-at (rx (repeat 3 (or "-" "+"))))
		(looking-back (rx (repeat 3 (or "-" "+"))))))
       (let* ((min (save-excursion (re-search-backward (rx "/*") nil t)))
	      (max (save-excursion (re-search-forward (rx "*/") nil t)))
	      (col (current-column))
	      (up (save-excursion
		    (while (and (> (point) min)
				(not (looking-at "-")))
		      (previous-line) 	; i'm not sure why just previous-line isn't good enough
		      (move-to-column col))
		    (looking-at "-")))
	      (down (save-excursion
		      (while (and (< (point) max)
				  (not (looking-at "-")))
			(next-line)
			(move-to-column col))
		      (looking-at "-")))
	      (right (save-excursion (re-search-forward "[^\\]|" (line-end-position) t)))
	      (left (save-excursion (re-search-backward "[^\\]|" (line-beginning-position) t))))
	 (and up down left right))))


(defun qmk--goto-string-in-cell ()
  "Find the string in the current cell and go to that line. It's
probably a good idea to use qmk--in-cell-p before trying to call
this."
  (let ((col (current-column)))
    (while (save-excursion
	     (previous-line) (move-to-column col)
	     (not (or (looking-at (rx (repeat 3 (or "-" "+"))))
		      (looking-back (rx (repeat 3 (or "-" "+")))))))
      (previous-line) (move-to-column col))
    (while (looking-at (rx (seq (+ space) (seq (not "\\") "|"))))
      (next-line) (move-to-column col))))


(define-minor-mode qmk-mode
  "A minor mode for editing layout.c files for QMK keyboards.
Functionality is provided to edit the matrix directly from the
code comment traditionally seen in QMK layout files."
  :lighter " qmk"
  :keymap `((,(kbd "C-. C-t") . qmk-cell-to-matrix)
	    (,(kbd "C-. C-.") . qmk-edit-cell-at-point)
	    (,(kbd "C-. C-f") . qmk-forward-cell)
	    (,(kbd "C-. C-b") . qmk-backward-cell)
	    (,(kbd "C-. C-p") . qmk-up-cell)
	    (,(kbd "C-. C-n") . qmk-down-cell)))


(provide 'qmk-mode)

;;; qmk-mode.el ends here


