#+title: qmk-mode

*still early in development*

This minor mode allows you to edit the QMK layer array just by editing the comment before. This helps ensure that the comment and the keyboard remain in sync. QMK mode also provides a more concise Emacs-like syntax unlike the verbose QMK =KC_KEY= syntax.

* TODO Screenshots

* TODO Installation

* Getting Started

To get started with qmk mode, you will need to tell qmk mode what your keyboard comment looks like. This can be done by setting the =qmk-keyboard-table= variable. For example for the Ergodox keyboard:

#+begin_src elisp
  (setq qmk-keyboard-table
        "
  ,--------------------------------------------------.           ,--------------------------------------------------.
  |    0   |   1  |   2  |   3  |   4  |   5  |   6  |           |   7  |   8  |   9  |  10  |  11  |  12  |   13   |
  |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
  |   14   |  15  |  16  |  17  |  18  |  19  |  20  |           |  21  |  22  |  23  |  24  |  25  |  26  |   27   |
  |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
  |   28   |  29  |  30  |  31  |  32  |  33  |------|           |------|  34  |  35  |  36  |  37  |  38  |   39   |
  |--------+------+------+------+------+------|  46  |           |  47  |------+------+------+------+------+--------|
  |   40   |  41  |  42  |  43  |  44  |  45  |      |           |      |  48  |  49  |  50  |  51  |  52  |   53   |
  `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
    |  54  |  55  |  56  |  57  |  58  |                                       |  59  |  60  |  61  |  62  |   63   |
    `----------------------------------'                                       `----------------------------------'
                                         ,-------------.       ,-------------.
                                         |  64  |  65  |       |  66  |   67   |
                                  ,------|------|------|       |------+--------+------.
                                  |      |      |  68  |       |  69  |        |      |
                                  |  70  |  71  |------|       |------|   74   |  75  |
                                  |      |      |  72  |       |  73  |        |      |
                                  `--------------------'       `----------------------'
  ")

#+end_src

Note that the relevant parts of that string are the pipe =|= characters and the numbers. Whitespace doesn't matter. Therefore the above could also have been written as:

#+begin_src elisp
  (setq qmk-keyboard-table "|0|1|2|3|4|5|6||7|8|9|10|11|12|13||||||14|15|16|17|18|19|20||21|22|23|24|25|26|27||||||||28|29|30|31|32|33||||34|35|36|37|38|39|||46||47|||40|41|42|43|44|45||||48|49|50|51|52|53||54|55|56|57|58||59|60|61|62|63||64|65||66|67||||||||68||69||||70|71||||74|75||||72||73|||")
#+end_src

* Commands

- =qmk-cell-to-matrix= (=C-. C-t=) :: Parses the contents of the cell at point and inserts the results into the layer after the end of the comment.

- =qmk-edit-cell-at-point= (=C-. C-.=) :: Edit the contents of the cell at point. After editing the contents of the current cell are inserted into the following layer.

- =qmk-forward-cell= (=C-. C-f=) :: Move to the cell to the right of the current cell. If supplied a prefix argument also edit the new cell.

- =qmk-backward-cell= (=C-. C-b=) :: Like =qmk-forward-cell= but to the left.

- =qmk-up-cell= (=C-. C-p=) :: Like =qmk-forward-cell= but up.

- =qmk-down-cell= (=C-. C-n=) :: Like =qmk-forward-cell= but down.

* Syntax

The text in each cell is processed before being inserted into the layer. This allows for more concise syntax than the standard QMK syntax. Note that if there is any text not recognized by qmk-mode then that text will be inserted into the layer without any change. The syntax of qmk-mode tries to be similar to that of Emacs keybindings.

** Basic Keycodes

Letters, numbers, and symbols are simply represented as themselves. Note that the letters must be lowercase as qmk-mode uses capital letters specially. Things that aren't letters (space, right, etc) are represented by the lowercased forms of QMK keycodes without the leading =KC_=. Either the long or abbreviated forms work so =enter= and =ent= both translate to =KC_ENT=.

See the [[https://docs.qmk.fm/#/keycodes_basic][list of basic QMK keycodes]].

** Modifiers

Modifiers are represented by capital letters. Available modifiers:

- =S= for shift
- =C= for control
- =A= for alt (aka Meta in Emacs)
- =G= for gui (aka Super)
- =M= for meh (control, shift, and alt)
- =H= for hyper (control, gui, shift, and alt); this is different from what Emacs calls hyper

If the modifier letter is the only thing contained in the cell then that key is pressed. To press a key with the modifier key pressed use Emacs-like syntax (=C-e=).

** Overrides

See [[https://docs.qmk.fm/#/feature_key_overrides][QMK key overrides]].

Only basic overrides are supported. The syntax is =<key><modifier><key>= where the first key sends the second key when the modifier key is pressed. For example, =6S(= sends a left parentheses upon pressing shift+6. This assumes that a key override array is already present in the file as it handles adding the override to that array.

*** TODO add tip for overriding the mod mask alist (i.e. what I did with space)

** TODO Mod-Tap Keys

** TODO Layers

** ACTIVE Examples

- =C= :: sends left control
- =C-a= :: hold left control and press a

** TODO Special QMK keys list

- dynamic macros
- capsword
- keylock

** TODO Adding my own syntax
